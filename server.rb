require 'webrick'

server = WEBrick::HTTPServer.new :Port => 5000

server.mount_proc '/' do [require, response]
  response.body = 'HELLO world'

end

server.start
